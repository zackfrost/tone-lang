pub use bimap;

pub use type_equals::*;

mod type_equals;

pub type BiMap<A, B> = bimap::BiBTreeMap<A, B>;
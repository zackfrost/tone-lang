pub trait TypeEquals {
    type Other;
}

impl<T> TypeEquals for T {
    type Other = Self;
}
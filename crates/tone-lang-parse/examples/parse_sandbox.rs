use std::fs;

use tone_lang_parse::tokenize;

fn main() {
    let in_path = std::env::current_dir()
        .unwrap()
        .join("test_files")
        .join("test.tone");
    let src = fs::read_to_string(in_path).unwrap();

    for t in tokenize(&src) {
        match t {
            Ok(t) => println!("{:?}", t),
            Err(e) => {
                panic!("\n{}\n", e);
            }
        };
    }
}

use tone_lang_parse::*;
use ast::*;

#[test]
fn test_tokenize_0() {
    let s = include_str!("data/test_0.tone");
    let expected: Vec<Token> = vec![
        Token::Reserved(ReservedToken::KwEntry),
        Token::Identifier("main".into()),
        Token::Reserved(ReservedToken::OpenParen),
        Token::Reserved(ReservedToken::CloseParen),
        Token::Reserved(ReservedToken::OpenCurly),
        Token::Identifier("string".into()),
        Token::Reserved(ReservedToken::ScopeResolution),
        Token::Identifier("println".into()),
        Token::Reserved(ReservedToken::OpenParen),
        Token::Literal(LiteralValue::Str("Hello, world!".into())),
        Token::Reserved(ReservedToken::CloseParen),
        Token::Reserved(ReservedToken::Semicolon),
        Token::Reserved(ReservedToken::CloseCurly),
    ];
    for (i, t) in tokenize(&s).enumerate() {
        assert!(i < expected.len());
        assert!(t.is_ok());
        assert_eq!(t.unwrap(), expected[i]);
    }
}

use quick_error::*;

quick_error! {
    #[derive(Debug)]
    pub enum ParseError {
        Unexpected(unexpected: String, line_number: usize) {
            display("Error on line {} -- Unexpected: {}", line_number, unexpected)
        }
        Missing(missing: String, line_number: usize) {
            display("Error on line {} -- Missing: {}", line_number, missing)
        }
    }
}

impl ParseError {
    pub fn from_unexpected(unexpected: String, line_num: usize) -> Self {
        Self::Unexpected(unexpected, line_num)
    }
    pub fn from_missing(missing: String, line_num: usize) -> Self {
        Self::Missing(missing, line_num)
    }
}

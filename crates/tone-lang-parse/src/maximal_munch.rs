use std::iter::Iterator;
use std::ops::Range;

/// Substitute for the `equal_range` function from C++
pub(crate) fn equal_range<T: Clone, F: Fn(T, T) -> bool>(
    elements: impl Iterator<Item=T>,
    range: Range<usize>,
    value: T,
    compare: F,
) -> (usize, usize) {
    let mut lower: usize = usize::MAX;
    let mut upper: usize = usize::MAX;

    for (idx, e) in elements.enumerate().skip(range.start) {
        if !range.contains(&idx) {
            break;
        }
        if (compare)(value.clone(), e.clone()) && upper == usize::MAX {
            upper = idx;
        }
        if !(compare)(e, value.clone()) && lower == usize::MAX {
            lower = idx;
        }
    }
    (lower, upper)
}

#[derive(PartialEq, Ord, Eq, Clone, Copy)]
pub(crate) enum MaximalMunchValue<'a> {
    Char(char),
    Str(&'a str),
}

impl<'a> PartialOrd for MaximalMunchValue<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match self {
            MaximalMunchValue::Char(_) => {}
            MaximalMunchValue::Str(l) => match other {
                MaximalMunchValue::Char(_) => {}
                MaximalMunchValue::Str(r) => {
                    return r.partial_cmp(l);
                }
            },
        }
        None
    }
}

pub(crate) fn maximal_munch_compare<'a>(
    idx: usize,
    l: MaximalMunchValue<'a>,
    r: MaximalMunchValue<'a>,
) -> bool {
    match l {
        MaximalMunchValue::Char(l) => match r {
            MaximalMunchValue::Char(r) => l < r,
            MaximalMunchValue::Str(r) => r.len() > idx && l < r.chars().nth(idx).unwrap(),
        },
        MaximalMunchValue::Str(l) => match r {
            MaximalMunchValue::Char(r) => l.len() <= idx || l.chars().nth(idx).unwrap() < r,
            MaximalMunchValue::Str(r) => {
                r.len() > idx
                    && (l.len() < idx || l.chars().nth(idx).unwrap() < r.chars().nth(idx).unwrap())
            }
        },
    }
}

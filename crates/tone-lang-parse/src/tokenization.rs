use std::collections::VecDeque;
use std::iter::Iterator;

use ast::*;

use crate::{get_keyword_or_type, get_operator, ParseError, PushBackStream};

#[derive(PartialEq)]
pub(crate) enum CharType {
    Punctuation,
    Whitespace,
    Identifier,
}

pub(crate) fn get_char_type(ch: char) -> CharType {
    if ch.is_ascii_whitespace() {
        CharType::Whitespace
    } else if ch.is_ascii_alphabetic() || ch.is_ascii_digit() || ch == '_' {
        CharType::Identifier
    } else {
        CharType::Punctuation
    }
}

fn rewind(stream: &mut PushBackStream, chars: &mut VecDeque<Option<char>>) {
    while let Some(Some(ch)) = chars.pop_back() {
        stream.push_back(ch);
    }
}

fn fetch_word(stream: &mut PushBackStream) -> Result<Token, ParseError> {
    let line_num = stream.get_line_number();

    let mut word = String::new();

    let mut chars = VecDeque::new();
    let mut ch = stream.next_char();
    if ch.is_none() {
        return Err(ParseError::from_unexpected(
            "{End-of-file}".into(),
            line_num,
        ));
    }
    let is_num = ch.unwrap().is_ascii_digit();
    let mut already_has_dot = false;
    let mut skip_push = false;
    loop {
        if let Some(ch) = ch {
            word.push(ch);
        }
        ch = stream.next_char();
        chars.push_back(ch);
        if let Some('.') = ch {
            if already_has_dot {
                rewind(stream, &mut chars);
                word = word.strip_suffix(".").unwrap_or(&word).into();
                skip_push = true;
                break;
            }
            already_has_dot = true;
        } else if ch.is_none() {
            break;
        } else if let Some(tmp_ch) = ch {
            if get_char_type(tmp_ch) != CharType::Identifier && !(is_num && tmp_ch == '.') {
                break;
            }
        }
    }
    if let Some(ch) = ch {
        if !skip_push {
            stream.push_back(ch);
        }
    }
    if let Some(tok) = get_keyword_or_type(&word, stream) {
        Ok(tok)
    } else if let Ok(val) = word.parse::<i64>() {
        Ok(Token::Literal(LiteralValue::Int(val)))
    } else if let Ok(val) = word.parse::<f64>() {
        Ok(Token::Literal(LiteralValue::Real(val)))
    } else {
        Ok(Token::Identifier(word))
    }
}

fn fetch_operator(stream: &mut PushBackStream) -> Result<Token, ParseError> {
    if let Some(tok) = get_operator(stream) {
        Ok(Token::Reserved(tok))
    } else {
        let line_num = stream.get_line_number();

        let unexpected = {
            let mut s = String::new();
            loop {
                if let Some(ch) = stream.next_char() {
                    if get_char_type(ch) != CharType::Punctuation {
                        break;
                    }
                    s.push(ch);
                }
            }
            s
        };
        Err(ParseError::from_unexpected(unexpected, line_num))
    }
}

fn fetch_string(stream: &mut PushBackStream) -> Token {
    let string = {
        let mut s = String::new();
        while let Some(ch) = stream.next_char() {
            if ch == '"' {
                break;
            } else {
                s.push(ch);
            }
        }
        s
    };

    Token::Literal(LiteralValue::Str(string))
}

fn skip_rest_of_line(stream: &mut PushBackStream) {
    while let Some(ch) = stream.next_char() {
        if ch == '\n' {
            break;
        }
    }
}

fn get_next_token(stream: &mut PushBackStream) -> Result<Token, ParseError> {
    loop {
        let c = stream.next_char();
        if c.is_none() {
            return Ok(Token::EndOfFile);
        }
        let c = c.unwrap();
        let t = get_char_type(c);
        match t {
            CharType::Punctuation => {
                if c == '"' {
                    return Ok(fetch_string(stream));
                } else if c == '#' {
                    skip_rest_of_line(stream);
                    continue;
                } else {
                    stream.push_back(c);
                    return fetch_operator(stream);
                }
            }
            CharType::Whitespace => continue,
            CharType::Identifier => {
                stream.push_back(c);
                return fetch_word(stream);
            }
        }
    }
}

struct Tokenizer {
    stream: PushBackStream,
}

impl Iterator for Tokenizer {
    type Item = Result<Token, ParseError>;

    fn next(&mut self) -> Option<Self::Item> {
        let r = get_next_token(&mut self.stream);
        if let Err(e) = r {
            Some(Err(e))
        } else {
            let token = r.unwrap();
            if let Token::EndOfFile = token {
                None
            } else {
                Some(Ok(token))
            }
        }
    }
}

pub fn tokenize(src: &str) -> impl Iterator<Item=Result<Token, ParseError>> {
    let src = format!("{}\n", src);
    let chars: Vec<_> = src.chars().collect();
    let stream = PushBackStream::new(chars);
    Tokenizer { stream }
}

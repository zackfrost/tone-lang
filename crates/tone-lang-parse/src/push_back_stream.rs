use std::collections::VecDeque;

pub struct PushBackStream {
    stack: VecDeque<char>,
    source: Vec<char>,
    line_num: usize,
    char_idx: usize,
}

impl PushBackStream {
    pub fn new(source: Vec<char>) -> Self {
        Self {
            stack: VecDeque::new(),
            source,
            line_num: 0,
            char_idx: 0,
        }
    }

    pub fn get_line_number(&self) -> usize {
        self.line_num
    }
    pub fn get_char_index(&self) -> usize {
        self.char_idx
    }

    pub fn next_char(&mut self) -> Option<char> {
        let ret;
        if self.stack.is_empty() {
            ret = self.source.first().copied();
            if !self.source.is_empty() {
                self.source.remove(0);
            }
        } else {
            ret = self.stack.pop_back();
        }
        if let Some(ch) = ret {
            if ch == '\n' {
                self.line_num += 1;
            }
        }
        self.char_idx += 1;
        ret
    }

    pub fn push_back(&mut self, ch: char) {
        self.stack.push_back(ch);
        if ch == '\n' {
            self.line_num -= 1;
        }
        self.char_idx -= 1;
    }
}

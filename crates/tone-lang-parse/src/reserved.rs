use std::collections::{BTreeMap, VecDeque};

use lazy_static::*;

use ast::{ReservedToken, Token, TypeToken};

use crate::{maximal_munch::*, PushBackStream};
use crate::tokenization::{CharType, get_char_type};

lazy_static! {
    static ref OPERATOR_TOKEN_MAP: BTreeMap<MaximalMunchValue<'static>, ReservedToken> = {
        let mut m = BTreeMap::new();
        m.insert(MaximalMunchValue::Str("++"), ReservedToken::Inc);
        m.insert(MaximalMunchValue::Str("--"), ReservedToken::Dec);
        m.insert(MaximalMunchValue::Str("+"), ReservedToken::Add);
        m.insert(MaximalMunchValue::Str("-"), ReservedToken::Sub);
        m.insert(MaximalMunchValue::Str("*"), ReservedToken::Mul);
        m.insert(MaximalMunchValue::Str("/"), ReservedToken::FDiv);
        m.insert(MaximalMunchValue::Str("//"), ReservedToken::IDiv);
        m.insert(MaximalMunchValue::Str("%"), ReservedToken::FMod);
        m.insert(MaximalMunchValue::Str("%%"), ReservedToken::IMod);
        m.insert(MaximalMunchValue::Str("~"), ReservedToken::BitNot);
        m.insert(MaximalMunchValue::Str("&"), ReservedToken::BitAnd);
        m.insert(MaximalMunchValue::Str("|"), ReservedToken::BitOr);
        m.insert(MaximalMunchValue::Str("^"), ReservedToken::BitXor);
        m.insert(MaximalMunchValue::Str("<<"), ReservedToken::ShiftL);
        m.insert(MaximalMunchValue::Str(">>"), ReservedToken::ShiftR);
        m.insert(MaximalMunchValue::Str("="), ReservedToken::Assign);
        m.insert(MaximalMunchValue::Str(".."), ReservedToken::Range);
        m.insert(MaximalMunchValue::Str("@"), ReservedToken::ScopeResolution);
        m.insert(MaximalMunchValue::Str("=>"), ReservedToken::Arrow);
        m.insert(MaximalMunchValue::Str("=="), ReservedToken::Equal);
        m.insert(MaximalMunchValue::Str("!="), ReservedToken::NotEqual);
        m.insert(MaximalMunchValue::Str("<"), ReservedToken::Less);
        m.insert(MaximalMunchValue::Str(">"), ReservedToken::Greater);
        m.insert(MaximalMunchValue::Str("<="), ReservedToken::LessEqual);
        m.insert(MaximalMunchValue::Str(">="), ReservedToken::GreaterEqual);
        m.insert(MaximalMunchValue::Str("."), ReservedToken::Dot);
        m.insert(MaximalMunchValue::Str(","), ReservedToken::Comma);
        m.insert(MaximalMunchValue::Str(";"), ReservedToken::Semicolon);
        m.insert(MaximalMunchValue::Str("("), ReservedToken::OpenParen);
        m.insert(MaximalMunchValue::Str(")"), ReservedToken::CloseParen);
        m.insert(MaximalMunchValue::Str("{"), ReservedToken::OpenCurly);
        m.insert(MaximalMunchValue::Str("}"), ReservedToken::CloseCurly);
        m.insert(MaximalMunchValue::Str("["), ReservedToken::OpenSquare);
        m.insert(MaximalMunchValue::Str("]"), ReservedToken::CloseSquare);
        m
    };
    static ref KEYWORD_TOKEN_MAP: BTreeMap<&'static str, ReservedToken> = {
        let mut m = BTreeMap::new();
        // Control structures
        m.insert("if", ReservedToken::KwIf);
        m.insert("else", ReservedToken::KwElse);
        m.insert("elif", ReservedToken::KwElseIf);
        m.insert("for", ReservedToken::KwFor);
        m.insert("in", ReservedToken::KwIn);

        // Flow control
        m.insert("break", ReservedToken::KwBreak);
        m.insert("continue", ReservedToken::KwContinue);
        m.insert("return", ReservedToken::KwReturn);

        // Declarations
        m.insert("entry", ReservedToken::KwEntry);
        m.insert("def", ReservedToken::KwDef);

        // Logical operators
        m.insert("not", ReservedToken::LogicalNot);
        m.insert("and", ReservedToken::LogicalAnd);
        m.insert("or", ReservedToken::LogicalOr);
        m
    };

    static ref TYPE_TOKEN_MAP: BTreeMap<&'static str, TypeToken> = {
        let mut m = BTreeMap::new();
        m.insert("void", TypeToken::Void);
        m.insert("f32", TypeToken::F32);
        m.insert("f64", TypeToken::F64);
        m.insert("u8", TypeToken::U8);
        m.insert("u32", TypeToken::U32);
        m.insert("u64", TypeToken::U64);
        m.insert("i8", TypeToken::I8);
        m.insert("i32", TypeToken::I32);
        m.insert("i64", TypeToken::I64);
        m.insert("bool", TypeToken::Bool);
        m.insert("str", TypeToken::Str);
        m
    };
    static ref LIST_TYPE_TOKEN: &'static str = "list";
}

fn skip_ws(stream: &mut PushBackStream, stack: &mut VecDeque<char>) -> Option<char> {
    loop {
        let c = {
            let tmp_c = stream.next_char();
            if let Some(c) = tmp_c {
                stack.push_back(c);
            }
            tmp_c
        };
        if let Some(c) = c {
            if get_char_type(c) != CharType::Whitespace {
                break Some(c);
            }
        } else {
            break None;
        }
    }
}

fn next_char(stream: &mut PushBackStream, stack: &mut VecDeque<char>) -> Option<char> {
    let c = stream.next_char();
    if let Some(c) = c {
        stack.push_back(c);
    }
    c
}

fn fetch_generic_type(stream: &mut PushBackStream) -> Option<Token> {
    let mut stack: VecDeque<char> = VecDeque::new();
    let mut ret: Option<Token> = None;

    let mut ch = { skip_ws(stream, &mut stack) };

    let found_open = {
        if let Some(ch) = ch {
            ch == '<'
        } else {
            false
        }
    };

    let mut word = String::new();
    if found_open {
        {
            ch = skip_ws(stream, &mut stack);
        }
        loop {
            if let Some(ch) = ch {
                word.push(ch);
            }
            {
                ch = next_char(stream, &mut stack);
            }
            if ch.is_none() {
                break;
            } else if let Some(tmp_ch) = ch {
                if get_char_type(tmp_ch) != CharType::Identifier {
                    break;
                }
            }
        }
    }

    let found_close = {
        if let Some(ch) = ch {
            ch == '>' && found_open
        } else {
            false
        }
    };

    if found_open && found_close {
        if let Some(t) = TYPE_TOKEN_MAP.get(word.as_str()) {
            ret = Some(Token::Type(TypeToken::List(Box::new(t.clone()))));
        }
    }

    if ret.is_none() {
        while let Some(c) = stack.pop_front() {
            stream.push_back(c);
        }
    }
    ret
}

pub fn get_keyword_or_type(word: &str, stream: &mut PushBackStream) -> Option<Token> {
    if word == *LIST_TYPE_TOKEN {
        fetch_generic_type(stream)
    } else if let Some(t) = TYPE_TOKEN_MAP.get(word) {
        Some(Token::Type(t.clone()))
    } else if let Some(&t) = KEYWORD_TOKEN_MAP.get(word) {
        Some(Token::Reserved(t))
    } else {
        None
    }
}

pub fn get_operator(stream: &mut PushBackStream) -> Option<ReservedToken> {
    let mut candidates = (0usize, OPERATOR_TOKEN_MAP.len());
    let candidates_vec: Vec<_> = OPERATOR_TOKEN_MAP.iter().collect();
    let mut ret = None;
    let mut chars: VecDeque<Option<char>> = VecDeque::new();
    let mut idx = 0usize;
    let mut match_size = 0usize;
    loop {
        if candidates.0 == candidates.1 {
            break;
        }
        let c = stream.next_char();
        chars.push_back(c);
        let compare = |l: (&MaximalMunchValue, &ReservedToken),
                       r: (&MaximalMunchValue, &ReservedToken)|
                       -> bool { maximal_munch_compare(idx, *l.0, *r.0) };

        candidates = equal_range(
            OPERATOR_TOKEN_MAP.iter(),
            candidates.0..candidates.1,
            (
                &MaximalMunchValue::Char(if let Some(v) = chars.back().unwrap() {
                    *v
                } else {
                    '\0'
                }),
                &ReservedToken::Add,
            ),
            compare,
        );
        if candidates.0 != candidates.1 {
            let v = candidates_vec[candidates.0];
            if let MaximalMunchValue::Str(s) = v.0 {
                if s.len() == idx + 1 {
                    match_size = idx + 1;
                    ret = Some(v.1);
                }
            }
        }
        idx += 1;
    }
    while chars.len() > match_size {
        let ch = if let Some(v) = chars.pop_back().unwrap() {
            v
        } else {
            '\0'
        };
        stream.push_back(ch);
    }
    ret.copied()
}

use std::cmp::Ordering;
use std::ops::{Add, Div, Mul, Rem, Sub};

#[derive(Debug, Copy, Clone, PartialEq)]
#[repr(u8)]
pub enum ValueType {
    Real,
    Int,
    Str,
    Bool,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    Real(f64),
    Int(i64),
    Str(String),
    Bool(bool),
}

impl Value {
    pub fn into_int(self) -> Option<Value> {
        match self {
            Value::Real(v) => Some(Self::Int(v.floor() as i64)),
            Value::Int(_) => Some(self),
            Value::Bool(v) => Some(Self::Int(v as i64)),
            _ => None,
        }
    }
}

impl PartialOrd for Value {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self {
            Value::Real(l) => match other {
                Value::Real(r) => l.partial_cmp(r),
                Value::Int(r) => l.partial_cmp(&(*r as f64)),
                Value::Str(_) => None,
                Value::Bool(_) => None,
            },
            Value::Int(l) => match other {
                Value::Real(r) => l.partial_cmp(&(*r as i64)),
                Value::Int(r) => l.partial_cmp(r),
                Value::Str(_) => None,
                Value::Bool(_) => None,
            },
            Value::Str(l) => match other {
                Value::Real(_) => None,
                Value::Int(_) => None,
                Value::Str(r) => l.partial_cmp(r),
                Value::Bool(_) => None,
            },
            Value::Bool(_) => None,
        }
    }
}

impl Add for Value {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        match self {
            Value::Real(l) => match rhs {
                Value::Real(r) => Value::Real(l + r),
                Value::Int(r) => Value::Real(l + r as f64),
                Value::Str(_) => unimplemented!(),
                Value::Bool(_) => unimplemented!(),
            },
            Value::Int(l) => match rhs {
                Value::Real(r) => Value::Real(l as f64 + r),
                Value::Int(r) => Value::Int(l + r),
                Value::Str(_) => unimplemented!(),
                Value::Bool(_) => unimplemented!(),
            },
            Value::Str(l) => match rhs {
                Value::Real(r) => Value::Str(format!("{}{}", l, r)),
                Value::Int(r) => Value::Str(format!("{}{}", l, r)),
                Value::Str(r) => Value::Str(format!("{}{}", l, r)),
                Value::Bool(r) => Value::Str(format!("{}{}", l, r)),
            },
            Value::Bool(_) => unimplemented!(),
        }
    }
}

impl Sub for Value {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        match self {
            Value::Real(l) => match rhs {
                Value::Real(r) => Value::Real(l - r),
                Value::Int(r) => Value::Real(l - r as f64),
                Value::Str(_) => unimplemented!(),
                Value::Bool(_) => unimplemented!(),
            },
            Value::Int(l) => match rhs {
                Value::Real(r) => Value::Real(l as f64 - r),
                Value::Int(r) => Value::Int(l - r),
                Value::Str(_) => unimplemented!(),
                Value::Bool(_) => unimplemented!(),
            },
            Value::Str(_) => unimplemented!(),
            Value::Bool(_) => unimplemented!(),
        }
    }
}

impl Mul for Value {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        match self {
            Value::Real(l) => match rhs {
                Value::Real(r) => Value::Real(l * r),
                Value::Int(r) => Value::Real(l * r as f64),
                Value::Str(_) => unimplemented!(),
                Value::Bool(_) => unimplemented!(),
            },
            Value::Int(l) => match rhs {
                Value::Real(r) => Value::Real(l as f64 * r),
                Value::Int(r) => Value::Int(l * r),
                Value::Str(_) => unimplemented!(),
                Value::Bool(_) => unimplemented!(),
            },
            Value::Str(_) => unimplemented!(),
            Value::Bool(_) => unimplemented!(),
        }
    }
}

impl Div for Value {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        match self {
            Value::Real(l) => match rhs {
                Value::Real(r) => Value::Real(l / r),
                Value::Int(r) => Value::Real(l / r as f64),
                Value::Str(_) => unimplemented!(),
                Value::Bool(_) => unimplemented!(),
            },
            Value::Int(l) => match rhs {
                Value::Real(r) => Value::Real(l as f64 / r),
                Value::Int(r) => Value::Real(l as f64 / r as f64),
                Value::Str(_) => unimplemented!(),
                Value::Bool(_) => unimplemented!(),
            },
            Value::Str(_) => unimplemented!(),
            Value::Bool(_) => unimplemented!(),
        }
    }
}

impl Rem for Value {
    type Output = Self;

    fn rem(self, rhs: Self) -> Self::Output {
        match self {
            Value::Real(l) => match rhs {
                Value::Real(r) => Value::Real(l % r),
                Value::Int(r) => Value::Real(l as f64 % r as f64),
                Value::Str(_) => unimplemented!(),
                Value::Bool(_) => unimplemented!(),
            },
            Value::Int(l) => match rhs {
                Value::Real(r) => Value::Real(l as f64 % r),
                Value::Int(r) => Value::Real(l as f64 % r as f64),
                Value::Str(_) => unimplemented!(),
                Value::Bool(_) => unimplemented!(),
            },
            Value::Str(_) => unimplemented!(),
            Value::Bool(_) => unimplemented!(),
        }
    }
}

impl Value {
    pub fn get_type(&self) -> ValueType {
        match &self {
            Value::Real(_) => ValueType::Real,
            Value::Int(_) => ValueType::Int,
            Value::Str(_) => ValueType::Str,
            Value::Bool(_) => ValueType::Bool,
        }
    }
}

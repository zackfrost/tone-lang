use num_derive::FromPrimitive;

use crate::ValueType;

#[derive(FromPrimitive, Copy, Clone)]
#[repr(u8)]
pub enum OpCode {
    LiteralReal = 0x01,
    SetReal = 0x02,
    GetReal = 0x03,

    LiteralInt = 0x04,
    SetInt = 0x05,
    GetInt = 0x06,

    LiteralBool = 0x07,
    SetBool = 0x08,
    GetBool = 0x09,

    LiteralStr = 0x0A,
    SetStr = 0x0B,
    GetStr = 0x0C,

    Add = 0x0D,
    Sub = 0x0E,
    Mul = 0x0F,
    FDiv = 0x10,
    FMod = 0x11,
    IDiv = 0x12,
    IMod = 0x13,

    Equal = 0x14,
    NotEqual = 0x15,
    Less = 0x16,
    LessEqual = 0x17,
    Greater = 0x18,
    GreaterEqual = 0x19,

    LogicNot = 0x1A,
    LogicAnd = 0x1B,
    LogicOr = 0x1C,

    GoTo = 0x3F,

    IfThen = 0x40,
    IfThenElse = 0x41,

    Print = 0x42,
    PrintLine = 0x43,
}

pub enum OpCodeSignature {
    Setter(ValueType),
    Getter(ValueType),
    Literal(ValueType),
    FlowControl,
    Nullary,
}

impl OpCode {
    pub fn get_signature(&self) -> OpCodeSignature {
        match &self {
            Self::LiteralReal => OpCodeSignature::Literal(ValueType::Real),
            Self::SetReal => OpCodeSignature::Setter(ValueType::Real),
            Self::GetReal => OpCodeSignature::Getter(ValueType::Real),

            Self::LiteralInt => OpCodeSignature::Literal(ValueType::Int),
            Self::SetInt => OpCodeSignature::Setter(ValueType::Int),
            Self::GetInt => OpCodeSignature::Getter(ValueType::Int),

            Self::LiteralStr => OpCodeSignature::Literal(ValueType::Str),
            Self::SetStr => OpCodeSignature::Setter(ValueType::Str),
            Self::GetStr => OpCodeSignature::Getter(ValueType::Str),

            Self::LiteralBool => OpCodeSignature::Literal(ValueType::Bool),
            Self::SetBool => OpCodeSignature::Setter(ValueType::Bool),
            Self::GetBool => OpCodeSignature::Getter(ValueType::Bool),

            Self::IfThen => OpCodeSignature::FlowControl,
            Self::IfThenElse => OpCodeSignature::FlowControl,

            _ => OpCodeSignature::Nullary,
        }
    }
    pub fn new(byte: u8) -> Option<Self> {
        num_traits::FromPrimitive::from_u8(byte)
    }
}

use std::collections::HashMap;
use std::io::Cursor;
use std::io::prelude::*;

use byteorder::{BigEndian, WriteBytesExt};

use crate::{OpCode, OpCodeSignature, Value, ValueType};

pub struct BytecodeBuilder {
    writer: Cursor<Vec<u8>>,
    labels: HashMap<String, u64>,

    // Maps bytecode locations referencing label offsets, to the label names
    labels_in_use: HashMap<u64, String>,
}

impl BytecodeBuilder {
    pub fn new() -> Self {
        Self {
            writer: Cursor::new(Vec::new()),
            labels: HashMap::new(),
            labels_in_use: HashMap::new(),
        }
    }

    pub fn add_label(mut self, name: &str) -> Self {
        self.labels.insert(name.into(), self.writer.position());
        self
    }

    pub fn push_nullary(mut self, code: OpCode) -> Self {
        if let OpCodeSignature::Nullary = code.get_signature() {
            self.writer.write_u8(code as u8).unwrap();
            self
        } else {
            panic!("Expected an OpCode for which `get_signiture()` returns: `OpCodeSignature::Nullary`");
        }
    }

    pub fn push_setter(mut self, code: OpCode, var_id: u32) -> Self {
        if let OpCodeSignature::Setter(_) = code.get_signature() {
            self.writer.write_u8(code as u8).unwrap();
            self.writer.write_u32::<BigEndian>(var_id).unwrap();
            self
        } else {
            panic!("Expected an OpCode for which `get_signiture()` returns: `OpCodeSignature::Setter(_)`");
        }
    }

    pub fn push_getter(mut self, code: OpCode, var_id: u32) -> Self {
        if let OpCodeSignature::Getter(_) = code.get_signature() {
            self.writer.write_u8(code as u8).unwrap();
            self.writer.write_u32::<BigEndian>(var_id).unwrap();
            self
        } else {
            panic!("Expected an OpCode for which `get_signiture()` returns: `OpCodeSignature::Getter(_)`");
        }
    }

    pub fn push_literal(mut self, value: Value) -> Self {
        let code = match value.get_type() {
            ValueType::Real => OpCode::LiteralReal,
            ValueType::Int => OpCode::LiteralInt,
            ValueType::Str => OpCode::LiteralStr,
            ValueType::Bool => OpCode::LiteralBool,
        };

        self.writer.write_u8(code as u8).unwrap();
        match value {
            Value::Real(v) => self.writer.write_f64::<BigEndian>(v).unwrap(),
            Value::Int(v) => self.writer.write_i64::<BigEndian>(v).unwrap(),
            Value::Str(v) => {
                let v = v.as_bytes();
                let n = v.len() as u64;
                self.writer.write_u64::<BigEndian>(n).unwrap();
                self.writer.write_all(v).unwrap();
            }
            Value::Bool(v) => self.writer.write_u8(v as u8).unwrap(),
        }
        self
    }

    pub fn push_if(mut self, then_branch_label: &str) -> Self {
        const CODE: OpCode = OpCode::IfThen;

        self.writer.write_u8(CODE as u8).unwrap();
        self.labels_in_use
            .insert(self.writer.position(), then_branch_label.into());
        self.writer.write_u64::<BigEndian>(0).unwrap();
        self
    }

    pub fn push_goto(mut self, dest_label: &str) -> Self {
        const CODE: OpCode = OpCode::GoTo;

        self.writer.write_u8(CODE as u8).unwrap();
        self.labels_in_use
            .insert(self.writer.position(), dest_label.into());
        self.writer.write_u64::<BigEndian>(0).unwrap();
        self
    }

    pub fn build(mut self) -> Vec<u8> {
        for (offset, name) in self.labels_in_use {
            self.writer.set_position(offset);
            let label_offset = self.labels.get(&name).expect("Label not found!");
            self.writer.write_u64::<BigEndian>(*label_offset).unwrap();
        }
        self.writer.into_inner()
    }
}

impl Default for BytecodeBuilder {
    fn default() -> Self {
        Self::new()
    }
}

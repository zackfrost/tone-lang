use std::cmp::Ordering;
use std::collections::VecDeque;
use std::error::Error;
use std::fmt;
use std::io::prelude::*;
use std::io::SeekFrom;

use byteorder::{BigEndian, ReadBytesExt};

use bytecode::{OpCode, OpCodeSignature, Value, ValueType};
cfg_if::cfg_if! {
    if #[cfg(feature = "fast-hash")] {
        use ahash::AHashMap as HashMap;
    } else {
        use std::collections::HashMap;
    }
}
#[derive(Debug, Clone)]
enum VmError {
    InvalidArgument,
    TypeMismatch,
    VariableTypeCantBeChanged,
    VariableNotFound,
    RequiredOperandMissing,
    StackError,
    InvalidOperation,
}

impl fmt::Display for VmError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self {
            Self::InvalidArgument => write!(f, "Invalid argument"),
            Self::TypeMismatch => write!(f, "Type mismatch"),
            Self::VariableTypeCantBeChanged => {
                write!(f, "Variable type can't be changed after creation")
            }
            Self::VariableNotFound => write!(f, "Variable not found"),
            Self::RequiredOperandMissing => write!(f, "Missing required operand(s)"),
            Self::StackError => write!(f, "Stack error"),
            Self::InvalidOperation => write!(f, "Invalid operation"),
        }
    }
}

impl Error for VmError {}

pub struct Vm<R: BufRead + Seek> {
    reader: R,
    stack: VecDeque<Value>,
    variables: HashMap<u32, Value>,
}

impl<R: BufRead + Seek> Vm<R> {
    pub fn new(reader: R) -> Self {
        Self {
            reader,
            stack: VecDeque::new(),
            variables: HashMap::new(),
        }
    }

    fn set_variable(&mut self, ty: ValueType) -> Result<(), Box<dyn std::error::Error>> {
        let mut id_buf = [0u8, 0u8, 0u8, 0u8];
        if let Err(e) = self.reader.read_exact(&mut id_buf) {
            return Err(e.into());
        }
        let id = u32::from_be_bytes(id_buf);

        let v = self.stack.pop_front();
        if v.is_none() {
            return Err(VmError::StackError.into());
        }
        let v = v.unwrap();
        if self.variables.contains_key(&id) {
            let old_ty = self.variables.get(&id).unwrap().get_type();
            if old_ty != ty {
                return Err(VmError::VariableTypeCantBeChanged.into());
            }
        }
        if v.get_type() != ty {
            return Err(VmError::TypeMismatch.into());
        }
        self.variables.insert(id, v);
        Ok(())
    }

    fn get_variable(&mut self, ty: ValueType) -> Result<(), Box<dyn std::error::Error>> {
        let mut id_buf = [0u8, 0u8, 0u8, 0u8];
        if let Err(e) = self.reader.read_exact(&mut id_buf) {
            return Err(e.into());
        }
        let id = u32::from_be_bytes(id_buf);

        if let Some(v) = self.variables.get(&id) {
            if ty != v.get_type() {
                return Err(VmError::TypeMismatch.into());
            }
            self.stack.push_back(v.clone());
        } else {
            return Err(VmError::VariableNotFound.into());
        }
        Ok(())
    }

    // TODO: Extract methods for each variant for increased performance
    fn push_literal(&mut self, ty: ValueType) -> Result<(), Box<dyn std::error::Error>> {
        let mut buf: Vec<u8> = match ty {
            ValueType::Real | ValueType::Int => {
                vec![0; 8]
            }
            ValueType::Bool => {
                vec![0]
            }
            ValueType::Str => {
                let mut len_buf = [0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8, 0u8];
                if let Err(e) = self.reader.read_exact(&mut len_buf) {
                    return Err(e.into());
                }
                let len = u64::from_be_bytes(len_buf);
                vec![0; len as usize]
            }
        };
        if let Err(e) = self.reader.read_exact(&mut buf) {
            return Err(e.into());
        }
        let v = match ty {
            ValueType::Real => Value::Real(f64::from_be_bytes([
                buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6], buf[7],
            ])),
            ValueType::Int => Value::Int(i64::from_be_bytes([
                buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6], buf[7],
            ])),
            ValueType::Str => {
                let s = String::from_utf8(buf);
                match s {
                    Ok(v) => Value::Str(v),
                    Err(e) => {
                        return Err(e.into());
                    }
                }
            }
            ValueType::Bool => Value::Bool(buf[0] != 0),
        };
        self.stack.push_back(v);
        Ok(())
    }

    fn exec_equal(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        self.stack.push_back(Value::Bool(
            lhs.partial_cmp(&rhs).unwrap() == Ordering::Equal,
        ));
        Ok(())
    }

    fn exec_not_equal(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        self.stack.push_back(Value::Bool(
            lhs.partial_cmp(&rhs).unwrap() != Ordering::Equal,
        ));
        Ok(())
    }

    fn exec_less(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        self.stack.push_back(Value::Bool(
            lhs.partial_cmp(&rhs).unwrap() == Ordering::Less,
        ));
        Ok(())
    }

    fn exec_greater(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        self.stack.push_back(Value::Bool(
            lhs.partial_cmp(&rhs).unwrap() == Ordering::Greater,
        ));
        Ok(())
    }

    fn exec_less_equal(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        let order = lhs.partial_cmp(&rhs).unwrap();
        self.stack.push_back(Value::Bool(
            order == Ordering::Less || order == Ordering::Equal,
        ));
        Ok(())
    }

    fn exec_greater_equal(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        let order = lhs.partial_cmp(&rhs).unwrap();
        self.stack.push_back(Value::Bool(
            order == Ordering::Greater || order == Ordering::Equal,
        ));
        Ok(())
    }

    fn exec_add(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        self.stack.push_back(lhs + rhs);
        Ok(())
    }

    fn exec_sub(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        self.stack.push_back(lhs - rhs);
        Ok(())
    }

    fn exec_mul(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        self.stack.push_back(lhs * rhs);
        Ok(())
    }

    fn exec_fdiv(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        self.stack.push_back(lhs / rhs);
        Ok(())
    }

    fn exec_idiv(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        if let Some(v) = (lhs / rhs).into_int() {
            self.stack.push_back(v);
            Ok(())
        } else {
            Err(VmError::InvalidOperation.into())
        }
    }

    fn exec_fmod(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        self.stack.push_back(lhs % rhs);
        Ok(())
    }

    fn exec_imod(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        if self.stack.len() < 2 {
            return Err(VmError::RequiredOperandMissing.into());
        }
        let lhs = self.stack.pop_front().unwrap();
        let rhs = self.stack.pop_front().unwrap();
        if let Some(v) = (lhs % rhs).into_int() {
            self.stack.push_back(v);
            Ok(())
        } else {
            Err(VmError::InvalidOperation.into())
        }
    }

    fn exec_print_line(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        let arg = self.stack.pop_front();
        println!("{}", Self::get_print_str(arg)?);
        Ok(())
    }

    fn exec_print(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        let arg = self.stack.pop_front();
        print!("{}", Self::get_print_str(arg)?);
        Ok(())
    }

    fn get_print_str(arg: Option<Value>) -> Result<String, Box<dyn std::error::Error>> {
        let s = match arg {
            Some(Value::Real(v)) => format!("{}", v),
            Some(Value::Int(v)) => format!("{}", v),
            Some(Value::Str(v)) => v,
            Some(Value::Bool(v)) => format!("{}", v),
            _ => {
                return Err(VmError::InvalidArgument.into());
            }
        };
        Ok(s)
    }

    fn handle_goto(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        match self.reader.read_u64::<BigEndian>() {
            Ok(p) => {
                if let Err(e) = self.reader.seek(SeekFrom::Start(p)) {
                    return Err(e.into());
                }
            }
            Err(e) => {
                return Err(e.into());
            }
        }
        Ok(())
    }

    fn handle_branch_then(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        let condition = self.stack.pop_front().unwrap();
        let pos = self.reader.read_u64::<BigEndian>().unwrap();
        if let Value::Bool(v) = condition {
            if v {
                self.reader.seek(SeekFrom::Start(pos))?;
            }
        } else {
            return Err(VmError::TypeMismatch.into());
        }
        Ok(())
    }

    fn handle_opcode_arguments(&mut self, code: OpCode) -> Result<(), Box<dyn std::error::Error>> {
        match code.get_signature() {
            OpCodeSignature::Nullary | OpCodeSignature::FlowControl => Ok(()),
            OpCodeSignature::Literal(ty) => self.push_literal(ty),
            OpCodeSignature::Setter(ty) => self.set_variable(ty),
            OpCodeSignature::Getter(ty) => self.get_variable(ty),
        }
    }

    fn handle_opcode(&mut self, code: OpCode) -> Result<(), Box<dyn std::error::Error>> {
        self.handle_opcode_arguments(code)?;
        match code {
            OpCode::GoTo => self.handle_goto(),
            OpCode::IfThen => self.handle_branch_then(),
            OpCode::PrintLine => self.exec_print_line(),
            OpCode::Print => self.exec_print(),
            OpCode::Equal => self.exec_equal(),
            OpCode::NotEqual => self.exec_not_equal(),
            OpCode::Less => self.exec_less(),
            OpCode::LessEqual => self.exec_less_equal(),
            OpCode::Greater => self.exec_greater(),
            OpCode::GreaterEqual => self.exec_greater_equal(),
            OpCode::Add => self.exec_add(),
            OpCode::Sub => self.exec_sub(),
            OpCode::Mul => self.exec_mul(),
            OpCode::FDiv => self.exec_fdiv(),
            OpCode::IDiv => self.exec_idiv(),
            OpCode::FMod => self.exec_fmod(),
            OpCode::IMod => self.exec_imod(),
            _ => Ok(()),
        }
    }

    fn advance(&mut self, skip: bool) -> Result<bool, Box<dyn std::error::Error>> {
        let mut buf = [0u8];
        let code = match self.reader.read(&mut buf) {
            Ok(_size) => OpCode::new(buf[0]),
            Err(e) => {
                return Err(e.into());
            }
        };
        if let Some(code) = code {
            if !skip {
                self.handle_opcode(code)?;
            }
        }
        Ok(code.is_some())
    }

    pub fn execute_all(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        loop {
            match self.advance(false) {
                Ok(r) => {
                    if !r {
                        break Ok(());
                    }
                }
                Err(e) => {
                    break Err(e);
                }
            }
        }
    }
}

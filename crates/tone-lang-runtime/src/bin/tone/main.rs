use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;

use clap::{App, Arg};

use tone_lang_runtime::*;

struct Args {
    input_file: PathBuf,
}

fn parse_args() -> Args {
    let app = App::new("Tone Runtime Environment")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Zachary Frost <zfzackfrost@pm.me>")
        .about("CPU-side runtime environment for the Tone scripting language")
        .arg(
            Arg::with_name("SCRIPT")
                .help("Required. The path to the compiled script to execute.")
                .required(true)
                .index(1),
        );
    let matches = app.clone().get_matches();
    Args {
        input_file: PathBuf::from(matches.value_of("SCRIPT").unwrap()),
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = parse_args();

    let file = match File::open(args.input_file) {
        Ok(f) => f,
        Err(e) => return Err(e.into()),
    };
    let mut vm = Vm::new(BufReader::new(file));
    vm.execute_all()?;
    Ok(())
}

use std::fs;
use std::io::Cursor;
use std::io::prelude::*;

use byteorder::{BigEndian, WriteBytesExt};

use bytecode::OpCode;

fn main() {
    const VAR_ID: u32 = 1;
    // Generate bytecode to print sequential integers
    let mut bytes = Cursor::new(Vec::new());

    bytes.write_u8(OpCode::LiteralInt as u8).unwrap();
    bytes.write_i64::<BigEndian>(0).unwrap();
    bytes.write_u8(OpCode::SetInt as u8).unwrap();
    bytes.write_u32::<BigEndian>(VAR_ID).unwrap();

    let start_offset = bytes.position();
    bytes.write_u8(OpCode::GetInt as u8).unwrap();
    bytes.write_u32::<BigEndian>(VAR_ID).unwrap();
    bytes.write_u8(OpCode::PrintLine as u8).unwrap();

    // Increment
    bytes.write_u8(OpCode::GetInt as u8).unwrap();
    bytes.write_u32::<BigEndian>(VAR_ID).unwrap();
    bytes.write_u8(OpCode::LiteralInt as u8).unwrap();
    bytes.write_i64::<BigEndian>(1).unwrap();
    bytes.write_u8(OpCode::Add as u8).unwrap();
    bytes.write_u8(OpCode::SetInt as u8).unwrap();
    bytes.write_u32::<BigEndian>(VAR_ID).unwrap();

    bytes.write_u8(OpCode::GetInt as u8).unwrap();
    bytes.write_u32::<BigEndian>(VAR_ID).unwrap();
    bytes.write_u8(OpCode::LiteralInt as u8).unwrap();
    bytes.write_i64::<BigEndian>(33).unwrap();
    bytes.write_u8(OpCode::GreaterEqual as u8).unwrap();
    bytes.write_u8(OpCode::IfThen as u8).unwrap();
    let branch_offset = bytes.stream_position().unwrap();
    bytes.write_u64::<BigEndian>(0).unwrap();

    bytes.write_u8(OpCode::GoTo as u8).unwrap();
    bytes.write_u64::<BigEndian>(start_offset).unwrap();

    let end_pos = bytes.stream_position().unwrap();
    //
    bytes.set_position(branch_offset);
    bytes.write_u64::<BigEndian>(end_pos).unwrap();

    let out_path = std::env::current_dir()
        .unwrap()
        .join("test_files")
        .join("test0.tnc");

    fs::write(out_path, bytes.get_ref()).unwrap();
}

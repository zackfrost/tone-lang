use std::fs;

use bytecode::{BytecodeBuilder, OpCode, Value};

fn main() {
    const VAR_I: u32 = 1;
    const VAR_PRV_PRV: u32 = 2;
    const VAR_PRV: u32 = 3;
    const VAR_CUR: u32 = 4;

    // Generate bytecode to print sequential fibonacci numbers
    let bytes = BytecodeBuilder::new()
        .add_label("INIT")
        .push_literal(Value::Int(0))
        .push_setter(OpCode::SetInt, VAR_I)
        .push_literal(Value::Int(0))
        .push_setter(OpCode::SetInt, VAR_PRV)
        .push_literal(Value::Int(0))
        .push_setter(OpCode::SetInt, VAR_PRV_PRV)
        .push_literal(Value::Int(1))
        .push_setter(OpCode::SetInt, VAR_CUR)

        .add_label("START")
        .push_getter(OpCode::GetInt, VAR_I)
        .push_literal(Value::Int(1))
        .push_nullary(OpCode::Add)
        .push_setter(OpCode::SetInt, VAR_I)

        .push_getter(OpCode::GetInt, VAR_PRV)
        .push_setter(OpCode::SetInt, VAR_PRV_PRV)

        .push_getter(OpCode::GetInt, VAR_CUR)
        .push_setter(OpCode::SetInt, VAR_PRV)

        .push_getter(OpCode::GetInt, VAR_PRV_PRV)
        .push_getter(OpCode::GetInt, VAR_CUR)
        .push_nullary(OpCode::Add)
        .push_setter(OpCode::SetInt, VAR_CUR)

        .push_getter(OpCode::GetInt, VAR_CUR)
        .push_nullary(OpCode::PrintLine)


        .push_getter(OpCode::GetInt, VAR_I)
        .push_literal(Value::Int(10))
        .push_nullary(OpCode::GreaterEqual)
        .push_if("END")
        .push_goto("START")
        .add_label("END")
        .build();

    let out_path = std::env::current_dir()
        .unwrap()
        .join("test_files")
        .join("test1.tnc");

    fs::write(out_path, bytes).unwrap();
}

use crate::ReservedToken;

#[derive(Debug, PartialEq)]
pub enum LiteralValue {
    Str(String),
    Real(f64),
    Int(i64),
    Bool(bool),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum VectorSize {
    D2,
    D3,
    D4,
}
impl VectorSize {
    pub fn len(&self) -> usize {
        match self {
            VectorSize::D2 => 2,
            VectorSize::D3 => 3,
            VectorSize::D4 => 4,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum MatrixSize {
    D2x2,
    D3x3,
    D4x4,
}

impl MatrixSize {
    pub fn get_row_size(&self) -> VectorSize {
        match self {
            MatrixSize::D2x2 => VectorSize::D2,
            MatrixSize::D3x3 => VectorSize::D3,
            MatrixSize::D4x4 => VectorSize::D4,
        }
    }
    pub fn get_col_size(&self) -> VectorSize {
        match self {
            MatrixSize::D2x2 => VectorSize::D2,
            MatrixSize::D3x3 => VectorSize::D3,
            MatrixSize::D4x4 => VectorSize::D4,
        }
    }

    pub fn row_len(&self) -> usize {
        self.get_row_size().len()
    }
    pub fn col_len(&self) -> usize {
        self.get_col_size().len()
    }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum TypeToken {
    Void,
    F32,
    F64,
    U8,
    U32,
    U64,
    I8,
    I32,
    I64,
    Bool,
    Str,
    List(Box<TypeToken>),
    Vector(Box<TypeToken>, VectorSize),
    Matrix(Box<TypeToken>, MatrixSize),
}

#[derive(Debug, PartialEq)]
pub enum Token {
    Identifier(String),
    EndOfFile,
    Literal(LiteralValue),
    Reserved(ReservedToken),
    Type(TypeToken),
}

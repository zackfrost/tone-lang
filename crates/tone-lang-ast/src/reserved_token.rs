#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum ReservedToken {
    Inc,
    Dec,

    Add,
    Sub,
    Mul,
    FDiv,
    IDiv,
    FMod,
    IMod,

    BitNot,
    BitAnd,
    BitOr,
    BitXor,

    ShiftL,
    ShiftR,

    Assign,
    Range,
    Arrow,

    ScopeResolution,

    LogicalNot,
    LogicalAnd,
    LogicalOr,

    Equal,
    NotEqual,
    Less,
    Greater,
    LessEqual,
    GreaterEqual,

    Dot,
    Comma,
    Semicolon,

    OpenParen,
    CloseParen,

    OpenCurly,
    CloseCurly,

    OpenSquare,
    CloseSquare,

    KwIf,
    KwElse,
    KwElseIf,

    KwFor,
    KwIn,

    KwBreak,
    KwContinue,
    KwReturn,

    KwEntry,
    KwDef,
}
